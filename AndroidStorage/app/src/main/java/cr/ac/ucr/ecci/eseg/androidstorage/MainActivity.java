package cr.ac.ucr.ecci.eseg.androidstorage;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonDBInsert = (Button) findViewById(R.id.buttonDBInsert);
        Button buttonDBSelect = (Button) findViewById(R.id.buttonDBSelect);
        Button buttonDBUpdate = (Button) findViewById(R.id.buttonDBUpdate);
        Button buttonDBDelete = (Button) findViewById(R.id.buttonDBDelete);
        Button buttonDBSave = (Button) findViewById(R.id.buttonDBSave);
        Button buttonDBLoad = (Button) findViewById(R.id.buttonDBLoad);
        buttonDBInsert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                insertarEstudiante();
            }
        });
        buttonDBSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerEstudiante();
            }
        });
        buttonDBDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                eliminarEstudiante();
            }
        });
        buttonDBUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                actualizarEstudiante();
            }
        });
        buttonDBSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                grabarArchivo();
            }
        });
        buttonDBLoad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerArchivo();
            }
        });

// almacenamiento de archivos internos
        Button buttonGrabarArchivo = (Button) findViewById(R.id.buttonDBSave);
        Button buttonLeerArchivo = (Button) findViewById(R.id.buttonDBLoad);
        buttonGrabarArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                grabarArchivo();
            }
        });

        buttonLeerArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerArchivo();
            }
        });
    }

    private void insertarEstudiante() {
        System.out.println("insertar estudiante");
// Instancia la clase Estudiante y realiza la inserción de datos
        Estudiante estudiante = new Estudiante("1-1000-1000",
                "estudiante01@ucr.ac.cr", "Juan",
                "Perez", "Soto", "2511-0000", "8890-0000",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148", 1,
                8.0);
// inserta el estudiante, se le pasa como parametro el contexto de la app
        long newRowId = estudiante.insertar(getApplicationContext());
        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Insertar Estudiante: " + newRowId +
                " Id: " + estudiante.getIdentificacion() +
                " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                estudiante.getNombre() +
                " " + estudiante.getPrimerApellido() + " " +
                estudiante.getSegundoApellido() +
                " Correo: " + estudiante.getCorreo() + " Tipo: " +
                estudiante.getTipo() + " Promedio: " +
                estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
    }

    private void leerEstudiante() {
// Instancia la clase Estudiante y realiza la lectura de datos
        Estudiante estudiante = new Estudiante();
// leer el estudiante, se le pasa como parametro el contexto de la app y la identificacion
        estudiante.leer(getApplicationContext(), "1-1000-1000");
// si lee al estudiante
        if (estudiante.getTipo().equals(Persona.TIPO_ESTUDIANTE)) {
// Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Leer Estudiante: " +
                    estudiante.getIdentificacion() +
                    " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                    estudiante.getNombre() +
                    " " + estudiante.getPrimerApellido() + " " +
                    estudiante.getSegundoApellido() +
                    " Correo: " + estudiante.getCorreo() + " Tipo: " +
                    estudiante.getTipo() + " Promedio: " +
                    estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "1-1000-1000",
                    Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarEstudiante() {
// Instancia la clase Estudiante y realiza el borrado de datos
        Estudiante estudiante = new Estudiante();
// leer el estudiante, se le pasa como parametro el contexto de la app y la identificacion
        estudiante.eliminar(getApplicationContext(), "1-1000-1000");
// Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Eliminar Estudiante.",
                Toast.LENGTH_LONG).show();
    }

    private void actualizarEstudiante() {
// Instancia la clase Estudiante y realiza la actualización de datos
        Estudiante estudiante = new Estudiante("1-1000-1000",
                "estudiante01@ucr.ac.cr*", "Juan*",
                "Perez*", "Soto*", "2511-0000*", "8890-0000*",
                UtilDates.StringToDate("01 01 1995"),
                Persona.TIPO_ESTUDIANTE, Persona.GENERO_MASCULINO, "A99148*", 1,
                8.0);
// actualiza el estudiante, se le pasa como parametro el contexto de la app
        int contador = estudiante.actualizar(getApplicationContext());
// si actualiza al estudiante
        if (contador > 0) {
// Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(), "Actualizar Estudiante: " +
                    contador + " Id: " + estudiante.getIdentificacion() +
                    " Carnet: " + estudiante.getCarnet() + " Nombre: " +
                    estudiante.getNombre() +
                    " " + estudiante.getPrimerApellido() + " " +
                    estudiante.getSegundoApellido() +
                    " Correo: " + estudiante.getCorreo() + " Tipo: " +
                    estudiante.getTipo() + " Promedio: " +
                    estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " +
                    estudiante.getIdentificacion(), Toast.LENGTH_LONG).show();
        }
    }

    public void grabarArchivo() {
// crear la persona
        Persona persona = new Persona("1-1000-1000", "estudiante01@ucr.ac.cr",
                "Juan", "Perez", "Soto*", "2511-0000", "8890-0000",
                UtilDates.StringToDate("01 01 1995"), Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO);
// grabar la persona en un archivo interno
        UtilFiles.guardarArchivoInterno(getApplicationContext(),
                "PersonaAndroidStorage.json", persona.toJson());
// mensaje al usuario
        Toast.makeText(getApplicationContext(), "Archivo creado: " +
                "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
    }

    // leer un archivo en almacenamiento interno
    public void leerArchivo() {
// leer el archivo
        String datosArchivo = datosArchivo =
                UtilFiles.leerArchivoInterno(getApplicationContext(),
                        "PersonaAndroidStorage.json");
        if (datosArchivo.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No hay datos para: " +
                    "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Archivo: " +
                    "PersonaAndroidStorage.json" +
                    "Contenido: " + datosArchivo, Toast.LENGTH_LONG).show();
        }
    }
}